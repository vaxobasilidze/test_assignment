<?php
  namespace Acme;
  class Connection {
    private $server; //Server Name
    private $uname; //User Name
    private $pass; //Password
    private $dbname; //Database Name
    protected function makeConnection(){
      $this->server = "localhost";
      $this->uname = "root";
      $this->pass = "";
      $this->dbname = "product";
      $connection = new \mysqli($this->server, $this->uname, $this->pass, $this->dbname);
      return $connection;
    }
  }
?>
