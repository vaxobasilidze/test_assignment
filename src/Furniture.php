<?php
  namespace Acme;
  class Furniture extends Product {
    private $height;
    private $width;
    private $length;
    public function setHeight($Height){ $this->height = $Height; }
    public function getHeight(){ return $this->height; }
    public function setWidth($Width){ $this->width = $Width; }
    public function getWidth(){ return $this->width; }
    public function setLength($Length){ $this->length = $Length; }
    public function getLength(){ return $this->length; }
    public function addProduct(){
      $connect = $this->makeConnection();
      $result = $connect->prepare("INSERT INTO products(type, sku, name, price, swd, companyid)VALUES(?,?,?,?,?,?)");
      $result->bind_param("sssdsi",$type,$sku,$name,$price,$swd,$companyId);
      $type = $this->getType();
      $sku = $this->getSku();
      $name = $this->getName();
      $price = $this->getPrice();
      $swd = $this->getHeight().'x'.$this->getWidth().'x'.$this->getLength();
      $companyId = $this->getCompany();
      $result->execute();
      if($result){ echo 1; }
      else { echo 0; }
    }
  }
?>
