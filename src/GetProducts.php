<?php
  namespace Acme;
  class GetProducts extends Connection {
    public function getProductsList(){
      $command = "SELECT products.id, products.type, products.sku, products.name, products.price, products.swd, companies.name AS cname from products LEFT JOIN companies ON companies.id = products.companyid";
      $result = $this->makeConnection()->query($command);
      if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
          $returnedData[] = $row;
        }
        return $returnedData;
      }
    }
  }
?>
