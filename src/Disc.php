<?php
  namespace Acme;
  class Disc extends Product {
    private $size;
    public function setSize($Size){ $this->size = $Size; }
    public function getSize(){ return $this->size; }
    public function addProduct(){
      $connect = $this->makeConnection();
      $result = $connect->prepare("INSERT INTO products(type, sku, name, price, swd, companyid)VALUES(?,?,?,?,?,?)");
      $result->bind_param("sssdsi",$type,$sku,$name,$price,$swd,$companyId);
      $type = $this->getType();
      $sku = $this->getSku();
      $name = $this->getName();
      $price = $this->getPrice();
      $swd = $this->getSize();
      $companyId = $this->getCompany();
      $result->execute();
      if($result){ echo 1; }
      else { echo 0; }
    }
  }
?>
