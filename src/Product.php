<?php
  namespace Acme;
  abstract class Product extends Connection {
    private $sku;
    private $name;
    private $price;
    private $type;
    private $company;
    public function setSku($Sku){ $this->sku = $Sku; }
    public function getSku(){ return $this->sku; }
    public function setName($Name){ $this->name = $Name; }
    public function getName(){ return $this->name; }
    public function setPrice($Price){ $this->price = $Price; }
    public function getPrice(){ return $this->price; }
    public function setType($Type){ $this->type = $Type; }
    public function getType(){ return $this->type; }
    public function setCompany($Company){ $this->company = $Company; }
    public function getCompany(){ return $this->company; }
    abstract public function addProduct();
  }
?>
