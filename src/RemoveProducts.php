<?php
  namespace Acme;
  class RemoveProducts extends Connection {
    private $removingProductId;
    public function setRemovingProductId($RemovingProductId){
      $this->removingProductId = $RemovingProductId;
    }
    public function getRemovingProductId(){
      return $this->removingProductId;
    }
    public function removeProduct(){
      $connect = $this->makeConnection();
      $remove = $connect->prepare("DELETE FROM products WHERE id = ?");
      $remove->bind_param("i",$id);
      $id = $this->getRemovingProductId();
      $result = $remove->execute();
      return $result;
    }
  }
?>
