<?php
  require 'vendor/autoload.php';
  use Acme\RemoveProducts;
  $ids = $_POST["ids"];
  $length = count($ids);
  for($i = 0; $i < $length; $i++){
    $removeProduct = new RemoveProducts();
    $id = (int)$ids[$i];
    $removeProduct->setRemovingProductId($id);
    $result = $removeProduct->removeProduct();
  }
  echo $result;
?>
