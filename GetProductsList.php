<?php
  require 'vendor/autoload.php';
  use Acme\GetProducts;
  $html = "";
  $productsList = new GetProducts();
  $products = $productsList->getProductsList(); //Retrieve products from database
  if($products != NULL){
    foreach ($products as $product) {
      $attribute = ($product["type"] == "book" ? "Weight" : ($product["type"] == "disc" ? "Size" : ($product["type"] == "furniture" ? "Dimentions" : "Description"))); //Find out what kind of product it is
      $entity = ($attribute == "Weight" ? "Kg" : ($attribute == "Size" ? "MB" : ""));
      $html .= '<div class="product product_' . $product["id"] . '" productid="' . $product["id"] . '">
        <div class="productdescription">
          <span>
            <input type="checkbox" name="check">
          </span>
          <table class="productdesc">
            <tr>
              <td>SKU:</td>
              <td>' . $product["sku"] . '</td>
            </tr>
            <tr>
              <td>Name:</td>
              <td>' . $product["name"] . '</td>
            </tr>
            <tr>
              <td>Company:</td>
              <td>' . $product["cname"] . '</td>
            </tr>
            <tr>
              <td>Price:</td>
              <td>' . $product["price"] . '$</td>
            </tr>
            <tr>
              <td>' . $attribute . ':</td>
              <td>' . $product["swd"] . ' ' . $entity . '</td>
            </tr>
          </table>
        </div>
      </div>';
    }
    echo($html);
  }
  else {
    echo "There are no products. Please create at least one new product to display them.";
  }
?>
