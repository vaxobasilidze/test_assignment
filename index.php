<?php
  require 'vendor/autoload.php';
  use Acme\GetCompanies;
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Products</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <body>
    <div id="maindiv">
      <div id="header">
        <h2>Product List</h2>
        <div class="actiondiv">
          <button type="button" name="add" class="addbtn btn" onclick="goToAddPage()">Add new product</button>
          <button type="button" name="remove" class="remove btn" onclick="removeProducts()">Remove selected</button>
        </div>
        <p class="clear"></p>
      </div>
      <div id="content">
        <div class="productslist">
          <?php require 'GetProductsList.php'; ?>
        </div>
        <div class="addproduct">
          <form class="productform" action="Submit.php" method="post">
            <table>
              <tr>
                <td>SKU:</td>
                <td>
                  <input type="text" name="productsku" class="productsku active inputfield">
                </td>
              </tr>
              <tr>
                <td>Name:</td>
                <td>
                  <input type="text" name="productname" class="productname active inputfield">
                </td>
              </tr>
              <tr>
                <td>Price ($):</td>
                <td>
                  <input type="text" name="productprice" class="productprice active inputfield">
                </td>
              </tr>
              <tr>
                <td>Choose Type:</td>
                <td>
                  <select name="typeselect" class="typeselect inputfield" onchange="typeSelect()">
                    <option value="disc" selected>Disc</option>
                    <option value="book">Book</option>
                    <option value="furniture">Furniture</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>Company:</td>
                <td>
                  <select name="companyselect" class="companyselect inputfield">
                    <?php
                      $companies = new GetCompanies();
                      $companiesList = $companies->getCompaniesList();
                      if($companiesList != NULL){
                        foreach ($companiesList as $company) {
                          $compId = $company["id"];
                          $compName = $company["name"];
                          $option = '<option value="'.$compId.'">'.$compName.'</option>';
                          echo ($option);
                        }
                      }
                    ?>
                  </select>
                </td>
              </tr>


              <!-- Disc field -->
              <tr class="disc-field">
                <td>Size:</td>
                <td>
                  <input type="text" name="disc-size" class="disc-size dynamic active inputfield">
                </td>
              </tr>
              <tr class="disc-field">
                <td colspan="2">
                  <span class="hint">Please enter disc size im Megabytes.</span>
                </td>
              </tr>

              <!-- Book field -->
              <tr class="book-field">
                <td>Weight:</td>
                <td>
                  <input type="text" name="book-weight" class="book-weight dynamic inputfield">
                </td>
              </tr>
              <tr class="book-field">
                <td colspan="2">
                  <span class="hint">Please enter book weight in Kg.</span>
                </td>
              </tr>

              <!-- Furniture fields -->
              <tr class="furniture-field">
                <td>Height:</td>
                <td>
                  <input type="text" name="furniture-height" class="furniture-height dimensions-field dynamic inputfield">
                </td>
              </tr>
              <tr class="furniture-field">
                <td>Width:</td>
                <td>
                  <input type="text" name="furniture-width" class="furniture-width dimensions-field dynamic inputfield">
                </td>
              </tr>
              <tr class="furniture-field">
                <td>Length:</td>
                <td>
                  <input type="text" name="furniture-length" class="furniture-length dimensions-field dynamic inputfield">
                </td>
              </tr>
              <tr class="furniture-field">
                <td colspan="2">
                  <span class="hint">Please enter furniture dimensions (Height, width and length in centimeters).</span>
                </td>
              </tr>

              <tr class="submitrow">
                <td colspan="2">
                  <input type="submit" name="submit" class="submit" value="Add product">
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <p class="message"></p>
                </td>
              </tr>
            </table>
          </form>
        </div>
      </div>
    </div>
    <script src="js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $(".productform").submit(function(e){
          e.preventDefault();
          var type = $('.typeselect').val();
          var productData = {
            productsku: $('.productsku').val(),
            productname: $('.productname').val(),
            productprice: $('.productprice').val(),
            productcompany: $('.companyselect').val(),
            typeselect: type
          }
          if(type == "furniture"){
            productData.height = $('.furniture-height').val();
            productData.width = $('.furniture-width').val();
            productData.length = $('.furniture-length').val();
          }
          else if(type == "book"){
            productData.weight = $('.book-weight').val();
          }
          else {
            productData.size = $('.disc-size').val();
          }
          $(".message").load("Submit.php", productData);
        });
      });
    </script>
  </body>
</html>
