<?php
  require 'vendor/autoload.php';
  use Acme\Disc;
  use Acme\Book;
  use Acme\Furniture;
  if($_POST["type"] == "disc"){
    $newProduct = new Disc();
    $newProduct->setSize($_POST["size"]);
  }
  else if($_POST["type"] == "book"){
    $newProduct = new Book();
    $newProduct->setWeight($_POST["weight"]);
  }
  else if($_POST["type"] == "furniture"){
    $newProduct = new Furniture();
    $newProduct->setHeight($_POST["height"]);
    $newProduct->setWidth($_POST["width"]);
    $newProduct->setLength($_POST["length"]);
  }
  $newProduct->setSku($_POST["sku"]);
  $newProduct->setName($_POST["name"]);
  $newProduct->setPrice($_POST["price"]);
  $newProduct->setType($_POST["type"]);
  $newProduct->setCompany($_POST["company"]);
  $status = $newProduct->addProduct();
  echo $status;
?>
